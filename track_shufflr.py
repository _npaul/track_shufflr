#!/usr/bin/env python3

import os
from pydub import AudioSegment
from pprint import pprint
import random
import json
import copy
import string

class Duration():
    def __init__(self, ms):
        self.ms = ms

    def __str__(self):
        secs = int(self.ms / 1000)
        mins = secs // 60
        secs = secs % 60
        if mins > 0:
            return f"{mins}:{secs:02d}"
        else:
            return f"{secs}s"

    def __add__(self, other):
        return Duration(self.ms + other.ms)

    @staticmethod
    def fromMins(mins):
        return Duration(mins * 60 * 1000)

class Track():
    def __init__(self, filename, audio_segment):
        self.name = filename.split('/')[-1].split('.')[0]
        self.filename = filename
        self.audio_segment = audio_segment
        self.length = Duration(len(self.audio_segment))

    def __str__(self):
        return f"Track({self.name}, {self.length})"

    @staticmethod
    def read(filename):
        print(f'Reading {filename}...')
        return Track(filename, AudioSegment.from_mp3(filename))


class Mix():
    def __init__(self, tracks=None):
        if tracks:
            self.tracks = tracks
        else:
            self.tracks = []

    def length(self):
        return sum((t.length for t in self.tracks), Duration(0))

    def __str__(self):
        return f"Mix({self.length()})[" + (', '.join(t.name for t in self.tracks)) + "]"

    def __repr__(self):
        return self.__str__()

    def uid(self):
        return '-'.join(t.name for t in self.tracks)

    def append(self, other):
        if isinstance(other, Track):
            self.tracks.append(other)
        elif isinstance(other, Mix):
            self.tracks.extend(other.tracks)
        else:
            raise ValueError()

    def isValid(self):
        ids = [t.name for t in self.tracks]
        if len(ids) == len(set(ids)):
            return True
        else:
            # print(f"Mix {self} is not unique")
            pass

    def export(self, dir, name=None):
        if name is None: name = self.uid()
        print(f'Exporting {name} (length={self.length()})...')
        mixdown = sum((track.audio_segment for track in self.tracks), AudioSegment.empty())
        mixdown.export(os.path.join(dir, f'{name}.mp3'), format='mp3')





class TrackSet():
    def __init__(self, tracks):
        self.tracks = {}
        for track in tracks:
            self.tracks[track.name] = track

    def __str__(self):
        return f"TrackSet[" + (', '.join(str(t) for t in self.tracks.values())) + "]"

    @staticmethod
    def readdir(dir, ext='.mp3', max=9e9):
        tracks = []
        files = os.listdir(dir)
        print(files)
        count = 0
        for f in files:
            if ext in f:
                tracks.append(Track.read(os.path.join(dir, f)))
                count += 1
                if count >= max: break
        return TrackSet(tracks)

    def split(self, structure):
        data = {}
        pprint(structure)
        for section_name, section in structure.items():
            mixes = []
            for trackset in section:
                mixes.append(Mix([self.tracks[t] for t in trackset]))
            data[section_name] = mixes

        return data

def randomize(begin, middle, end, length, thresh=None):
    if thresh is None: thresh = Duration.fromMins(5)
    # Select a begin and end
    begin_mix = random.choice(begin)
    end_mix = random.choice(end)
    dur = begin_mix.length() + end_mix.length()
    count = 0

    while True:
        middle_options = copy.copy(middle)
        middle_sections = random.randint(1,7)
        mix = Mix()
        mix.append(begin_mix)
        for i in range(random.randint(2, 7)):
            t = random.choice(middle_options)
            middle_options.remove(t)
            mix.append(t)
            if len(middle_options) == 0: break

        mix.append(end_mix)

        if abs(mix.length().ms - length.ms) < thresh.ms and mix.isValid():
            return mix
        else:
            if count != 0 and count % 100 == 0:
                print(f'{count} invalid attempts...')

            if count == 1000:
                print('Cound not find valid mix after 1000 attempts')
                return None

        count += 1

def randName():
    name = ''
    for _ in range(16):
        name += random.choice(string.ascii_letters)
    return name

def make_random_tracks(track_dir, config_file, length, thresh, count=10, export=True):
    all_tracks = TrackSet.readdir(track_dir)
    print(all_tracks)

    with open(config_file) as f:
        config = json.load(f)
    data = all_tracks.split(config)

    pprint(data)

    exported = 0
    while exported < count:
        mix = randomize(data['begin'],
                data['middle'],
                data['end'],
                length=length,
                thresh=thresh
                )

        if mix:
            exported += 1
            # print(mix)
            if export:
                mix.export('exports', name=randName())


def main():
    make_random_tracks('tracks', 'test.json',
                       length=Duration.fromMins(10),
                       thresh=Duration.fromMins(5),
                       count=2,
                       export=True)


if __name__ == '__main__':
    main()

